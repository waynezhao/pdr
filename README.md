#============================================================
# Printed Diagram Recognition (PDR) 
#============================================================
This is a course research project I am working on for computer vision class
(Spring 2014). The goal of this project is to take a scanned copy of a printed
diagram and output it as some canonical format to some software (e.g. visio).
Using the software, the printed diagram can be reconstruct. 
