#include<iostream>
#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>

#include"preProcess.h"
#include"shapeDetect.h"
#include"textExtract.h"
#include"topoRelation.h"
#include"diagram.h"

const char* wndName="printed diagram recognition";

int main(int argc, char *argv[]) {
    if(argc!=2) {
        std::cerr<<"Usage: "<<argv[0]<<" <Image File>"<<std::endl;
        return 1;
    }
    cv::Mat tmpImg = cv::imread(argv[1], CV_LOAD_IMAGE_GRAYSCALE);
    // make sure img has even number of columns and rows
    cv::Mat img = tmpImg(cv::Rect(0,0,tmpImg.cols/2*2, tmpImg.rows/2*2));
    if(!img.data) {
        std::cout <<"Failed to open image: "<<argv[1]<<std::endl;
        return 1;
    }
    // image preprocessing
    cv::Mat processedImg = preProcess(img);

    // show original image
    cv::namedWindow(wndName, 1);
    std::cout << "Showing Original Image" << std::endl;
    cv::imshow(wndName, img);
    cv::waitKey();
    cv::imwrite("original.png", img);
    // show processed image
    std::cout << "Showing Image After Preprocessing" << std::endl;
    cv::imshow(wndName, processedImg);
    cv::waitKey();
    cv::imwrite("processed.png", processedImg);

    // show detected shapes
    shapeDetect detector(processedImg);
    detector.getShapes();
    std::vector<myEllipse> elist = detector.ellipseList;
    detector.showShapes(img);

    // get shape mask
    cv::Mat shapeMask=detector.getMask(1,true);
    cv::Mat toShow = cv::Mat::zeros(img.rows, img.cols, CV_8U);
    processedImg.copyTo(toShow, shapeMask);

    // mask without shapes
    cv::Mat skeletonMask=detector.getMask(0, true);
    toShow = cv::Mat::zeros(img.rows, img.cols, CV_8U);
    img.copyTo(toShow, skeletonMask);
    toShow.setTo(cv::Scalar(255), shapeMask);
    std::cout << "Showing Detected Edges" << std::endl;
    cv::imshow(wndName, toShow);
    cv::waitKey();
    cv::imwrite("edges.png", toShow);
    // get out text
    textExtract extractor(detector, img);
    extractor.getTexts(detector);

    // figure out relationship
    assignRelation relationship(toShow);
    relationship.getRelation(extractor);

    // create diagram text output
    diagram myDiagram(extractor, relationship);
    myDiagram.outputDotFile();
    return 0;
}

