//----------------------------------------------------------------------------
// files containing possible ways to do image preprocessing
//-------------
// Reference:
//-------------
//  [1] OPENCV/samples/cpp/squares.cpp
//  [2] OPENCV/samples/cpp/fitellipse.cpp
//  OPENCV stands for opencv installation folder
//============================================================
// Weiran (Ryan) Zhao 
// Computer Science Dept, IU Bloomington
// Started: Sun,Apr 27th 2014 09:28:14 PM EDT
// Last Modified: Wed,May 07th 2014 05:16:24 AM EDT
//----------------------------------------------------------------------------

#ifndef _PRE_PROCESS__H__
#define _PRE_PROCESS__H__ 
#include<opencv2/core/core.hpp>

// threshold for canny edge detector
#define CANNY_LOW_THRESH 5
#define CANNY_HIGH_THRESH 50


cv::Mat preProcess(const cv::Mat& img);

#endif
