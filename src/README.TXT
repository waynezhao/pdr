---------------------
Dependent libraries
---------------------
1)  OpenCV (2.4.8)
2)  Tesseract OCR (3.0.2)
3)  Graphviz (2.38.0)
----------
Platform
----------
Code tested under Mac OS 10.9.2
I tested my code under "silo", silo only has OpenCV, Not OpenCV2, code won't
compile through.
I also tested my code under my Ubuntu desktop, it has OpenCV (2.3.*), same
parameters give significant different results under that library. 
If you modify the "Makefile" to add the correct Include directory and lib
directory, it may compile through under Linux machine, but depending on the
version of your libraries, you might NOT be able to reproduce my result. In
this case, I can give you a demo using my own machine.
------------
How to run
------------
If you are able to compile my code. After compilation, 
1)  run "./pdr imgs/<some image>"
2)  A window will pop out, let that window has focus, and press enter or space
to let it progress; it will show you
    a)  original image
    b)  image after preprocessing
    c)  image with all shapes detected
    d)  image with all edges detected
    e)  image with all texts extracted
    f)  connected component detected for edges
A few lines will show on terminal about progress of program, a file called
"result.dot" will be created, which contains DOT description of target
diagram. A few "*.png" images will be created, they contain intermediate
result of my program.
c)  run "./compare", "result.dot" will get compiled into "result.png", and
show together with "original.png".
d)  "make clean" will clean "*.o", "make kleen" will clean everything.

