//----------------------------------------------------------------------------
// Class definitions for the shapes I will recognize, including:
//  [1] rectangle
//  [2] diamond
//  [3] triangle
//  [4] ellispe
//============================================================
// Weiran (Ryan) Zhao 
// Computer Science Dept, IU Bloomington
// Started: Sun,Apr 27th 2014 11:03:01 PM EDT
// Last Modified: Wed,May 07th 2014 09:10:16 PM EDT
//----------------------------------------------------------------------------
#ifndef _MY_SHAPES__H__
#define _MY_SHAPES__H__ value
//#include<opencv2/imgproc/imgproc.hpp>
#include<opencv2/core/core.hpp>
#include<string>
#include<vector>

#ifndef MY_PI
#define MY_PI 3.1415926535897932
#endif
#define CENTER_ALLOWANCE_RATIO 0.04
#define AREA_ALLOWANCE_RATIO 0.3
#define SHINK_RATIO 0.95

#define ENLARGE_RATIO 0.4
#define ELLIPSE_SHRINK_RATIO 1.0
class myEllipse: public cv::RotatedRect {
    public:
        std::string myText;
        // bounding box for text
        cv::Rect boundingBox;
        // enlarged ellipse
        cv::RotatedRect enlargedEllipse;

        myEllipse(const cv::RotatedRect& rect);
        myEllipse& operator=(const myEllipse &other);
        bool quiteSimilar(const myEllipse &ellipse);
};

class myRect {
    public:
        cv::Point center;
        std::string myText;
        std::vector<cv::Point> vertices;
        // bounding box for text
        cv::Rect boundingBox;
        // vertices for enlarged rect
        std::vector<cv::Point> enlargedVertices;

        myRect(const std::vector<cv::Point> &vert);
        myRect& operator=(const myRect &other);
        bool quiteSimilar(const myRect &rect);

};

class myDiamond {
    public:
        cv::Point center;
        std::string myText;
        std::vector<cv::Point> vertices;
        // bounding box for text
        cv::Rect boundingBox;
        // vertices for enlarged diamond
        std::vector<cv::Point> enlargedVertices;

        myDiamond(const std::vector<cv::Point> &vert);
        myDiamond& operator=(const myDiamond &other);
        bool quiteSimilar(const myDiamond &diam);
};

#endif
