//----------------------------------------------------------------------------
// This class is for detecting simple shapes of a diagram.
// Currently allowed shapes are:
//  [1] rectangles
//  [2] ellipses
//  [3] diamonds
//  [4] triangles (indicating direction at the end of curve)
//  [5] simple curves (straight lines or with a bit curvature)
//-------------
// Reference:
//-------------
//  [1] OPENCV/samples/cpp/squares.cpp
//  [2] OPENCV/samples/cpp/fitellipse.cpp
//  OPENCV stands for opencv installation folder
//============================================================
// Weiran (Ryan) Zhao 
// Computer Science Dept, IU Bloomington
// Started: Sun,Apr 27th 2014 10:01:38 PM EDT
// Last Modified: Wed,May 07th 2014 05:21:41 AM EDT
//----------------------------------------------------------------------------
#ifndef _SHAPE_DETECT__H__
#define _SHAPE_DETECT__H__ 
#include<opencv2/core/core.hpp>
#include"myShapes.h"

// used by approxPolyDP
#define APPROX_ACCURACY 0.02
// used by isEllipse()
#ifndef MY_PI
#define MY_PI 3.1415926535897932
#endif
#define ELLIPSE_LOW_RATIO 0.95
#define ELLIPSE_HIGH_RATIO 1.05
#define ELLIPSE_AREA_MIN_RATIO 0.003
#define ELLIPSE_AREA_MAX_RATIO 0.5

// used by isRectangle()
// cos(80Deg) = 0.17364817766693041
// cos(70Deg) = 0.3420201433256688
#define RECT_MAX_COS 0.3
#define RECT_AREA_MIN_RATIO 0.003
#define RECT_AREA_MAX_RATIO 0.5

// used by isDiamond()
#define DIAMOND_MAX_DEG 5
#define DIAMOND_AREA_MIN_RATIO 0.005
#define DIAMOND_AREA_MAX_RATIO 0.3

// used by isTriangle
#define TRIANGLE_AREA_MAX_RATIO 0.005

// total number of binarize levels
#define BINARIZE_LEVELS 11
class shapeDetect{
    public:
        cv::Mat processedImg;
        std::vector<myEllipse> ellipseList;
        std::vector<myRect> rectList;
        std::vector<myDiamond> diamondList;

        // constructors
        shapeDetect(const cv::Mat &img);
        shapeDetect(const shapeDetect &sp);
        shapeDetect& operator= (const shapeDetect &sp);

        // extract shapes from image
        void getShapes();
        // show detected shapes
        void showShapes(const cv::Mat &img);
        // get mask of image based on type
        // Currently support:
        //      [1] 0 for skeleton
        //      [2] 1 for shapes
        // if enlarged is set to true, return skeleton with enlarged shapes
        cv::Mat getMask(int type, bool enlarged);

    private:
        // if contour is an ellipse
        bool isEllipse(const std::vector<cv::Point>& cont);
        // if contour is a rectangle
        bool isRectangle(const std::vector<cv::Point>& cont);
        // if contour is a diamond 
        bool isDiamond(const std::vector<cv::Point>& cont);
};
#endif
