//----------------------------------------------------------------------------
// Class definitions for the shapes I will recognize, including:
//  [1] rectangle
//  [2] diamond
//  [3] triangle
//  [4] ellispe
//============================================================
// Weiran (Ryan) Zhao 
// Computer Science Dept, IU Bloomington
// Started: Sun,Apr 27th 2014 11:03:01 PM EDT
// Last Modified: Wed,May 07th 2014 09:20:16 PM EDT
//----------------------------------------------------------------------------
#include"myShapes.h"
#include<cmath>
#include<iostream>
#include<cassert>
#include<cstring>
#include<cstdlib>
#include<algorithm>
#include<opencv2/imgproc/imgproc.hpp>

myEllipse::myEllipse(const cv::RotatedRect& rect): cv::RotatedRect(rect) {
    this->size.width *= SHINK_RATIO;
    this->size.height *= SHINK_RATIO;
    boundingBox = this->boundingRect();
    //cv::Rect tmp = this->boundingRect();
    //boundingBox.x = tmp.x+tmp.width*(1-ELLIPSE_SHRINK_RATIO)/2;
    //boundingBox.y = tmp.y+tmp.height*(1-ELLIPSE_SHRINK_RATIO)/2;
    //boundingBox.width = tmp.width*ELLIPSE_SHRINK_RATIO;
    //boundingBox.height= tmp.height*ELLIPSE_SHRINK_RATIO;
    // create enlarged ellipse
    cv::Size tmpSz;
    tmpSz.height = this->size.height*(1+ENLARGE_RATIO);
    tmpSz.width = this->size.width*(1+ENLARGE_RATIO);
    enlargedEllipse.size = tmpSz;
    enlargedEllipse.center = this->center;
    enlargedEllipse.angle = this->angle;
}

myEllipse& myEllipse::operator=(const myEllipse &other) {
    memcpy((cv::RotatedRect*) this, (cv::RotatedRect*) &other, sizeof(cv::RotatedRect));
    this->boundingBox = other.boundingBox;
    this->myText = other.myText;
    this->enlargedEllipse = other.enlargedEllipse;
    return *this;
}

bool myEllipse::quiteSimilar(const myEllipse &ellipse) {
    //--------------------
    // center difference
    //--------------------
    double centerDiff;
    centerDiff = (center.x-ellipse.center.x)*(center.x-ellipse.center.x)
        +(center.y-ellipse.center.y)*(center.y-ellipse.center.y);
    if(centerDiff>(CENTER_ALLOWANCE_RATIO*boundingRect().area()*MY_PI/4)) {
        return false;
    } else { // else check area difference
        double areaDiffRatio;
        double areaDiff = this->boundingRect().area() - ellipse.boundingRect().area();
        areaDiffRatio = 2*fabs(areaDiff)/(this->boundingRect().area()+ellipse.boundingRect().area());
        if(areaDiffRatio > AREA_ALLOWANCE_RATIO) {
            return false;
        }
        //---------------------------------
        // if similar keep the smaller one
        //---------------------------------
        if(areaDiff>0) {
            (*this) = ellipse;
        }
        return true;
    }
}

myRect::myRect(const std::vector<cv::Point> &vert) {
    // only can consist of 4 vertices
    assert(vert.size()==4);
    //-------------------------
    // store it as a cv::Rect
    //-------------------------
    // find the center of rectangle first
    for(size_t i=0; i<vert.size();i++) {
        center+= vert[i];
    }
    center.x /= vert.size();
    center.y /= vert.size();
    // use center and vertices to find height and width of rectangle
    double tmpWidth=0, tmpHeight=0;
    for(size_t i=0; i<vert.size();i++) {
        tmpWidth = std::max(tmpWidth, fabs(vert[i].x-center.x));
        tmpHeight = std::max(tmpHeight, fabs(vert[i].y-center.y));
    }
    tmpWidth *= SHINK_RATIO;
    tmpHeight *= SHINK_RATIO;
    cv::Size rectSize(2*tmpWidth, 2*tmpHeight);
    cv::Point topLeft(center.x-tmpWidth, center.y-tmpHeight);
    boundingBox=cv::Rect(topLeft, rectSize);
    //--------------------------------
    // store this list of 4 vertices
    //--------------------------------
    vertices.push_back(boundingBox.tl());
    vertices.push_back(boundingBox.tl()+cv::Point(boundingBox.width,0));
    vertices.push_back(boundingBox.br());
    vertices.push_back(boundingBox.br()-cv::Point(boundingBox.width,0));
    //--------------------------
    // store enlarged vertices
    //--------------------------
    cv::Rect tmpBox(boundingBox);
    tmpBox.x -= tmpBox.width*ENLARGE_RATIO/2;
    tmpBox.y -= tmpBox.height*ENLARGE_RATIO/2;
    tmpBox.height *= (1+ENLARGE_RATIO);
    tmpBox.width *= (1+ENLARGE_RATIO);
    enlargedVertices.push_back(tmpBox.tl());
    enlargedVertices.push_back(tmpBox.tl()+cv::Point(tmpBox.width,0));
    enlargedVertices.push_back(tmpBox.br());
    enlargedVertices.push_back(tmpBox.br()-cv::Point(tmpBox.width,0));
}

myRect& myRect::operator=(const myRect &other) {
    this->myText = other.myText;
    this->boundingBox = other.boundingBox;
    vertices.clear();
    for(size_t i=0; i<other.vertices.size();i++) {
        vertices.push_back(other.vertices[i]);
    }
    enlargedVertices.clear();
    for(size_t i=0; i<other.enlargedVertices.size();i++) {
        enlargedVertices.push_back(other.enlargedVertices[i]);
    }
    return *this;
}

bool myRect::quiteSimilar(const myRect &rect) {
    //----------------------------------
    // check topleft corner difference
    //----------------------------------
    double ptDiff = (boundingBox.x-rect.boundingBox.x)*(boundingBox.x-rect.boundingBox.x)
        +(boundingBox.y-rect.boundingBox.y)*(boundingBox.y-rect.boundingBox.y);
    if(ptDiff > CENTER_ALLOWANCE_RATIO * cv::contourArea(cv::Mat(vertices), true)) {
        return false;
    } else { // else check area difference
        double areaDiffRatio;
        double areaDiff = boundingBox.area()-rect.boundingBox.area();
        areaDiffRatio = 2*fabs(areaDiff)/(boundingBox.area()+rect.boundingBox.area());
        if(areaDiffRatio > AREA_ALLOWANCE_RATIO) {
            return false;
        }
        //---------------------------------
        // if similar keep the smaller one
        //---------------------------------
        if(areaDiff>0) {
            (*this) = rect;
        }
        return true;
    }
}

myDiamond::myDiamond(const std::vector<cv::Point> &vert) {
    // make sure there are only 4 vertices
    assert(vert.size()==4);
    // find the center of diamond 
    for(size_t i=0; i<vert.size();i++) {
        center+= vert[i];
    }
    center.x /= vert.size();
    center.y /= vert.size();
    // use vertices to find width and height diamond 
    double tmpWidth=0, tmpHeight=0;
    for(size_t i=0; i<vert.size(); i++) {
        tmpWidth = std::max(tmpWidth, fabs(vert[i].x-center.x));
        tmpHeight = std::max(tmpHeight, fabs(vert[i].y-center.y));
    }
    tmpWidth *= SHINK_RATIO;
    tmpHeight *= SHINK_RATIO;
    cv::Size rectSize(2*tmpWidth, 2*tmpHeight);
    cv::Point topLeft(center.x-tmpWidth, center.y-tmpHeight);
    boundingBox=cv::Rect(topLeft, rectSize);
    //--------------------------------
    // store this list of 4 vertices
    //--------------------------------
    vertices.push_back(boundingBox.tl()+cv::Point(0.5*boundingBox.width,0));
    vertices.push_back(boundingBox.br()-cv::Point(0, 0.5*boundingBox.height));
    vertices.push_back(boundingBox.br()-cv::Point(0.5*boundingBox.width,0));
    vertices.push_back(boundingBox.tl()+cv::Point(0,0.5*boundingBox.height));
    //--------------------------
    // store enlarged vertices
    //--------------------------
    cv::Rect tmpBox(boundingBox);
    tmpBox.x -= tmpBox.width*ENLARGE_RATIO/2;
    tmpBox.y -= tmpBox.height*ENLARGE_RATIO/2;
    tmpBox.height *= (1+ENLARGE_RATIO);
    tmpBox.width *= (1+ENLARGE_RATIO);
    enlargedVertices.push_back(tmpBox.tl()+cv::Point(0.5*tmpBox.width,0));
    enlargedVertices.push_back(tmpBox.br()-cv::Point(0, 0.5*tmpBox.height));
    enlargedVertices.push_back(tmpBox.br()-cv::Point(0.5*tmpBox.width,0));
    enlargedVertices.push_back(tmpBox.tl()+cv::Point(0,0.5*tmpBox.height));
    //-------------------------------------
    // have to use a smaller bounding box
    //-------------------------------------
    //boundingBox.x += boundingBox.width*0.25;
    //boundingBox.y += boundingBox.height*0.25;
    //boundingBox.width/=2;
    //boundingBox.height/=2;
}

myDiamond& myDiamond::operator=(const myDiamond &other) {
    this->myText = other.myText;
    this->boundingBox = other.boundingBox;
    vertices.clear();
    for(size_t i=0; i<other.vertices.size();i++) {
        vertices.push_back(other.vertices[i]);
    }
    enlargedVertices.clear();
    for(size_t i=0; i<other.enlargedVertices.size();i++) {
        enlargedVertices.push_back(other.enlargedVertices[i]);
    }
    return *this;
}
bool myDiamond::quiteSimilar(const myDiamond &diam) {
    //----------------------------------
    // check topleft corner difference
    //----------------------------------
    double ptDiff = (boundingBox.x-diam.boundingBox.x)*(boundingBox.x-diam.boundingBox.x)
        +(boundingBox.y-diam.boundingBox.y)*(boundingBox.y-diam.boundingBox.y);
    if(ptDiff > CENTER_ALLOWANCE_RATIO * cv::contourArea(cv::Mat(vertices), true)) {
        return false;
    } else { // else check area difference
        double areaDiffRatio;
        double areaDiff = boundingBox.area()-diam.boundingBox.area();
        areaDiffRatio = fabs(areaDiff)*2/(boundingBox.area()+diam.boundingBox.area());
        if(areaDiffRatio > AREA_ALLOWANCE_RATIO) {
            return false;
        }
        //---------------------------------
        // if similar keep the smaller one
        //---------------------------------
        if(areaDiff>0) {
            (*this) = diam;
        }
        return true;
    }
}
