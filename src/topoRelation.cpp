//----------------------------------------------------------------------------
// This class is used to figure out topological relationships between different
// shapes.
//-------------
// Reference:
//-------------
//  [1] OPENCV/sampels/cpp/connected_components.cpp
//  OPENCV stands for opencv installation folder
//============================================================
// Weiran (Ryan) Zhao 
// Computer Science Dept, IU Bloomington
// Started: Tue,Apr 29th 2014 09:07:26 PM EDT
// Last Modified: Wed,May 07th 2014 09:38:28 PM EDT
//----------------------------------------------------------------------------
#include"topoRelation.h"
#include"preProcess.h"
#include<opencv2/highgui/highgui.hpp>
#include<opencv2/imgproc/imgproc.hpp>
#include<iostream>
#include<map>
#include<limits>

extern char* wndName;

curveEnd::curveEnd(std::string t, cv::Point p, cv::Point tc) {
    myText = t;
    pos = p;
    textCenter = tc;
}

curveEnd::curveEnd(const curveEnd &other) {
    if(this!=&other) {
        this->myText = other.myText;
        this->pos = other.pos;
        this->textCenter = other.textCenter;
    }

}

curveEnd& curveEnd:: operator=(const curveEnd& other) {
    if(this!=&other) {
        this->myText = other.myText;
        this->pos = other.pos;
        this->textCenter = other.textCenter;
    }
    return *this;
}

relation::relation(curveEnd p, curveEnd c, int t):parent(p),child(c) {
    type = t;
}

relation::relation(const relation &other) {
    if(this!=&other) {
        this->parent  = other.parent;
        this->child = other.child;
        this->type = other.type;
    }
}

relation& relation:: operator=(const relation& other) {
    if(this!= &other) {
        this->parent  = other.parent;
        this->child = other.child;
        this->type = other.type;
    }
    return *this;
}

assignRelation::assignRelation(const cv::Mat &img) {
    skelImg = img;
}

// Helper function
static bool ptCompareByHeight(cv::Point i,cv::Point j) {
    //return (i.x*i.x+i.y*i.y) < (j.x*j.x+j.y*j.y);
    return i.y < j.y;
}
static bool ptCompareByWidth(cv::Point i,cv::Point j) {
    //return (i.x*i.x+i.y*i.y) < (j.x*j.x+j.y*j.y);
    return i.x < j.x;
}

// Ref: [1]
void assignRelation::getRelation(const textExtract& extractor) {
    //cv::imshow(wndName, skelImg);
    //cv::waitKey();
    cv::Mat processedImg = preProcess(skelImg);
    // dilate this image
    cv::dilate(processedImg, processedImg, cv::Mat(), cv::Point(-1,-1));
    //cv::imshow(wndName, processedImg);
    //cv::waitKey();
    // binarized image
    cv::Mat binImg = processedImg;
    cv::Mat demo = cv::Mat::zeros(binImg.size(), CV_8UC3);
    // use a white background
    demo.setTo(cv::Scalar(255, 255, 255));

    //-------------------------------------
    // start finding connected components
    //-------------------------------------
    std::vector<std::vector<cv::Point> > contours;
    std::vector<cv::Vec4i> hierarchy;
    // contours that can be considers as curves
    std::vector<std::vector<cv::Point> > curves;
    // use contour info
    findContours(binImg, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE );
    int curveCnter=0;
    if( !contours.empty() && !hierarchy.empty() ) {
        for(int i=0; i>= 0; i = hierarchy[i][0] ) {
            //-----------------------------------------------
            // use heuristics to rule out impossible curves
            //-----------------------------------------------
            // number of points
            if(contours[i].size()< 5) {
                continue;
            }
            cv::Scalar color( (rand()&255), (rand()&255), (rand()&255) );
            drawContours(demo , contours, i, color, CV_FILLED, 8, hierarchy );
            curves.push_back(contours[i]);
            curveCnter++;
        }
    }
    std::cout << "Showing connected component" << std::endl;
    cv::imshow(wndName, demo);
    cv::waitKey();
    cv::imwrite("connected.png", demo);

    //--------------------------
    // create center, text map
    //--------------------------
    // string can be used as key because no nodes can have same text
    std::map<std::string, cv::Point> textMap;
    // insert ellipseList
    for(size_t i=0; i<extractor.ellipseList.size(); i++) {
        textMap[extractor.ellipseList[i].myText] = extractor.ellipseList[i].center;
    }
    // insert rectangleList 
    for(size_t i=0; i<extractor.rectList.size(); i++) {
        textMap[extractor.rectList[i].myText] = extractor.rectList[i].center;
    }
    // insert diamondList 
    for(size_t i=0; i<extractor.diamondList.size(); i++) {
        textMap[extractor.diamondList[i].myText] = extractor.diamondList[i].center;
    }

    //---------------------------
    // Assign node to end point
    //---------------------------
    // for each curve, sort along width (x-axis)
    for(size_t i=0; i<curves.size(); i++) {
        cv::Rect bounds = cv::boundingRect(curves[i]);
        if(bounds.width > bounds.height) {
            std::sort(curves[i].begin(), curves[i].end(), ptCompareByWidth);
        } else {
            std::sort(curves[i].begin(), curves[i].end(), ptCompareByHeight);
        }
        //---------------------------------------
        // process two points and find the text
        //---------------------------------------
        cv::Point endPt1 = curves[i][0], endPt2 = curves[i][curves[i].size()-1];
        std::string text1 = nearestText(endPt1, textMap);
        std::string text2 = nearestText(endPt2, textMap);
        // create relation
        curveEnd ce1(text1, endPt1, textMap[text1]);
        curveEnd ce2(text2, endPt2, textMap[text2]);
        relation rel(ce1, ce2);
        // push relaiton to list
        relationList.push_back(rel);
    }
}

// helper function
static double ptDist(cv::Point p1, cv::Point p2) {
    double dist=0;
    dist += (p1.x-p2.x)*(p1.x-p2.x);
    dist += (p1.y-p2.y)*(p1.y-p2.y);
    return dist;
}
std::string assignRelation::nearestText(cv::Point pt, const std::map<std::string, cv::Point>& textMap) {
    std::string result;
    double minDist = std::numeric_limits<double>::max();
    double tmpDist;
    for(std::map<std::string, cv::Point>::const_iterator it=textMap.begin(); it!=textMap.end(); it++) {
        tmpDist = ptDist(pt, it->second);
        if(tmpDist < minDist) {
            result = it->first;
            minDist = tmpDist;
        }
    }
    return result;
}
