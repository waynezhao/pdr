//----------------------------------------------------------------------------
// This class is used to figure out topological relationships between different
// shapes.
//-------------
// Reference:
//-------------
//  [1] OPENCV/sampels/cpp/connected_components.cpp
//  OPENCV stands for opencv installation folder
//============================================================
// Weiran (Ryan) Zhao 
// Computer Science Dept, IU Bloomington
// Started: Tue,Apr 29th 2014 09:07:26 PM EDT
// Last Modified: Wed,May 07th 2014 03:14:04 AM EDT
//----------------------------------------------------------------------------
#ifndef _TOPO_RELATION__H__
#define _TOPO_RELATION__H__ 
#include"myShapes.h"
#include"textExtract.h"
#include<vector>
#include<string>
#include<opencv2/core/core.hpp>

// class define end point and corresponding text
class curveEnd {
    public:
        std::string myText;
        cv::Point textCenter;
        cv::Point pos;

        // constructors
        curveEnd() {}
        curveEnd(std::string t, cv::Point p, cv::Point tc);
        curveEnd(const curveEnd &other);
        curveEnd& operator=(const curveEnd& other);
};

// this class defines relation between shapes
class relation {
    public:
        // in case this is a undirected graph, parent and child no difference
        curveEnd parent; 
        curveEnd child;
        // 0 for undirected relation, 1 for directed relation
        int type;

        // constructors
        relation() {}
        relation(curveEnd p, curveEnd c, int t=0);
        relation(const relation &other);
        relation& operator=(const relation& other);
};

// this class assign list of relationship between pair of shapes
class assignRelation {
    public:
        cv::Mat skelImg;            // skeleton image containg only lines/curves
        std::vector<relation> relationList;

        // constructor
        assignRelation(const cv::Mat &img);
        // figure out relationship
        void getRelation(const textExtract& extractor);
        // given a curve end point, find the nearest text (node)
        std::string nearestText(cv::Point pt, const std::map<std::string,cv::Point>& textMap);

};
#endif
