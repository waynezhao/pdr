//----------------------------------------------------------------------------
// files containing possible ways to do image preprocessing
//-------------
// Reference:
//-------------
//  [1] OPENCV/samples/cpp/squares.cpp
//  [2] OPENCV/samples/cpp/fitellipse.cpp
//  OPENCV stands for opencv installation folder
//============================================================
// Weiran (Ryan) Zhao 
// Computer Science Dept, IU Bloomington
// Started: Sun,Apr 27th 2014 09:28:14 PM EDT
// Last Modified: Wed,May 07th 2014 05:23:20 AM EDT
//----------------------------------------------------------------------------
#include"preProcess.h"
#include<opencv2/imgproc/imgproc.hpp>
#include<opencv2/highgui/highgui.hpp>

// use window name
extern char* wndName;

cv::Mat preProcess(const cv::Mat& img) {
    cv::Mat tmp1, tmp2;
    //--------------------------------
    // downscale and upscale methods
    //--------------------------------
    cv::pyrDown(img, tmp1, cv::Size(img.cols/2, img.rows/2));
    cv::pyrUp(tmp1, tmp2, img.size());
    //cv::imshow(wndName, tmp2);
    //cv::waitKey();
    // or Gaussian filtering
    int kernelSz = 5;
    //tmp2 = img;
    //cv::GaussianBlur(img, tmp2, cv::Size(kernelSz,kernelSz),0);
    
    //--------------------------
    // use Canny edge detector
    //--------------------------
    cv::Canny(tmp2, tmp1, CANNY_LOW_THRESH, CANNY_HIGH_THRESH, kernelSz);
    //cv::imshow(wndName, tmp1);
    //cv::waitKey();

    return tmp1;
}
