//----------------------------------------------------------------------------
// This class is used for extracting text using *tesseract ocr* based on
// rectangular, diamond, and ellipse detected.
//--------------
// Reference: 
//--------------
//  [1] https://code.google.com/p/tesseract-ocr/wiki/APIExample
//      API example code
//============================================================
// Weiran (Ryan) Zhao 
// Computer Science Dept, IU Bloomington
// Started: Mon,Apr 28th 2014 12:29:00 PM EDT
// Last Modified: Wed,May 07th 2014 08:19:29 PM EDT
//----------------------------------------------------------------------------
#include"textExtract.h"
#include<opencv2/highgui/highgui.hpp>
#include<iostream>
#include<tesseract/baseapi.h>
#include<opencv2/opencv.hpp>
#include<cstring>

// use imshow window
extern char* wndName;

textExtract::textExtract(const shapeDetect& detector, const cv::Mat& img) {
    for(size_t i=0; i<detector.ellipseList.size(); i++) {
        ellipseList.push_back( detector.ellipseList[i]);
    }
    for(size_t i=0; i<detector.rectList.size(); i++) {
        rectList.push_back( detector.rectList[i]);
    }
    for(size_t i=0; i<detector.diamondList.size(); i++) {
        diamondList.push_back( detector.diamondList[i]);
    }
    orgImg = img;
}


// Main refrence [1]
void textExtract::getTexts(shapeDetect &detector) {
    //-----------------
    // initialize ocr
    //-----------------
    tesseract::TessBaseAPI *ocrAPI = new tesseract::TessBaseAPI();
    // initialize with english
    if (ocrAPI->Init(NULL, "eng")) {
        std::cerr<<"Could not initialize tesseract"<<std::endl;
        exit(1);
    }
    // get image with only shapes
    // get skeleton
    cv::Mat shapeMask=detector.getMask(1,false);
    cv::Mat toShow = cv::Mat::zeros(orgImg.rows, orgImg.cols, CV_8U);
    toShow.setTo(cv::Scalar(255));
    orgImg.copyTo(toShow, shapeMask);
    std::cout << "Showing Texts" << std::endl;
    cv::imshow(wndName, toShow);
    cv::waitKey();
    cv::imwrite("texts.png", toShow);

    // set image
    ocrAPI->SetImage(toShow.data, toShow.size().width, toShow.size().height, toShow.channels(), toShow.step1());
    //----------------------------------------
    // loop through each possible shape list
    //----------------------------------------
    cv::Rect tmpRect;
    char* text;
    std::string tmp;
    // rectangle list
    for(size_t i=0; i<rectList.size();i++) {
        tmpRect = rectList[i].boundingBox;
        // set ocr focus
        ocrAPI->SetRectangle(tmpRect.x, tmpRect.y, tmpRect.width, tmpRect.height);
        // get output
        text = ocrAPI->GetUTF8Text();
        //-----------------------------
        // assign string to rectangle
        //-----------------------------
        tmp = std::string(text);
        // removing traing newlines
        tmp.erase(std::remove(tmp.begin(), tmp.end(), '\n'), tmp.end());
        rectList[i].myText = tmp;
        // clean mess
        delete [] text;
    }
    // diamond list
    for(size_t i=0; i<diamondList.size();i++) {
        tmpRect = diamondList[i].boundingBox;
        // set ocr focus
        ocrAPI->SetRectangle(tmpRect.x, tmpRect.y, tmpRect.width, tmpRect.height);
        // get output
        text = ocrAPI->GetUTF8Text();
        // assign string to diamond
        tmp = std::string(text);
        // removing traing newlines
        tmp.erase(std::remove(tmp.begin(), tmp.end(), '\n'), tmp.end());
        diamondList[i].myText = tmp;
        // clean mess
        delete [] text;
    }
    // ellipse list
    for(size_t i=0; i<ellipseList.size();i++) {
        tmpRect = ellipseList[i].boundingBox;
        // set ocr focus
        ocrAPI->SetRectangle(tmpRect.x, tmpRect.y, tmpRect.width, tmpRect.height);
        // get output
        text = ocrAPI->GetUTF8Text();
        // assign string to diamond
        tmp = std::string(text);
        // removing traing newlines
        tmp.erase(std::remove(tmp.begin(), tmp.end(), '\n'), tmp.end());
        ellipseList[i].myText = tmp;
        // clean mess
        delete [] text;
    }

    //--------------
    // ocr destroy
    //--------------
    ocrAPI->End();

    //-----------------
    // print to check
    //-----------------
    std::cout << "=========Detected Texts===========" << std::endl;
    for(size_t i=0; i<rectList.size();i++) {
        std::cout << rectList[i].myText << std::endl;
    }
    for(size_t i=0; i<diamondList.size();i++) {
        std::cout << diamondList[i].myText << std::endl;
    }
    for(size_t i=0; i<ellipseList.size();i++) {
        std::cout << ellipseList[i].myText << std::endl;
    }
    std::cout << "==================================" << std::endl;
}


