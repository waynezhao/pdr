//----------------------------------------------------------------------------
// This file contains definition for class diagram, which mainly handles output
// to `graphviz` dot file format
//============================================================
// Weiran (Ryan) Zhao 
// Computer Science Dept, IU Bloomington
// Started: Tue,Apr 29th 2014 09:49:08 PM EDT
// Last Modified: Wed,Apr 30th 2014 01:45:34 AM EDT
//----------------------------------------------------------------------------
#ifndef _DIAGRAM__H__
#define _DIAGRAM__H__ 
#include<vector>
#include"myShapes.h"
#include"topoRelation.h"

//-------------------------------------------------------------
// this class contains shape info and relation between shapes
//-------------------------------------------------------------
class diagram {
    public:
        std::vector<myEllipse> ellipseList;
        std::vector<myRect> rectList;
        std::vector<myDiamond> diamondList;
        std::vector<relation> relationList;

        // constructors
        diagram(const std::vector<myEllipse> &eList, const std::vector<myRect> &rctList, const std::vector<myDiamond> &dList, const std::vector<relation> &rlnList);
        diagram(const textExtract& extractor, const assignRelation& relation);

        // output dot file
        void outputDotFile(std::string fName=std::string("result.dot"));

};

#endif
