//----------------------------------------------------------------------------
// This file contains definition for class diagram, which mainly handles output
// to `graphviz` dot file format
//============================================================
// Weiran (Ryan) Zhao 
// Computer Science Dept, IU Bloomington
// Started: Tue,Apr 29th 2014 09:49:08 PM EDT
// Last Modified: Wed,May 07th 2014 04:39:40 AM EDT
//----------------------------------------------------------------------------
#include"diagram.h"
#include<fstream>
#include<iostream>

diagram::diagram(const std::vector<myEllipse> &eList, const std::vector<myRect> &rctList, const std::vector<myDiamond> &dList, const std::vector<relation> &rlnList) {
    for(size_t i=0; i<eList.size(); i++) {
        ellipseList.push_back(eList[i]);
    }
    for(size_t i=0; i<rctList.size(); i++) {
        rectList.push_back(rctList[i]);
    }
    for(size_t i=0; i<dList.size(); i++) {
        diamondList.push_back(dList[i]);
    }
    for(size_t i=0; i<rlnList.size(); i++) {
        relationList.push_back(rlnList[i]);
    }
}

diagram::diagram(const textExtract& extractor, const assignRelation& relation) {
    for(size_t i=0; i<extractor.ellipseList.size(); i++) {
        ellipseList.push_back(extractor.ellipseList[i]);
    }
    for(size_t i=0; i<extractor.rectList.size(); i++) {
        rectList.push_back(extractor.rectList[i]);
    }
    for(size_t i=0; i<extractor.diamondList.size(); i++) {
        diamondList.push_back(extractor.diamondList[i]);
    }
    for(size_t i=0; i<relation.relationList.size(); i++) {
        relationList.push_back(relation.relationList[i]);
    }
}
void diagram::outputDotFile(std::string fName) {
    std::ofstream fileDot(fName.c_str(), std::ofstream::out);
    if(fileDot.is_open()) {
        //-------------
        // file start
        //-------------
        fileDot << "digraph G { " <<std::endl;
        fileDot << "splines=true;"<<std::endl;
        fileDot << "sep=\"+25,25\";"<<std::endl;
        fileDot << "overlap=scalexy;"<<std::endl;
        fileDot << "nodesep=0.6;"<<std::endl;

        //-------------
        // all shapes
        //-------------
        for(size_t i=0; i<ellipseList.size(); i++) {
            // name
            fileDot << "\"" << ellipseList[i].myText << "\"";
            // position
            fileDot << "[ pos=\"" << ellipseList[i].center.x <<","<<-ellipseList[i].center.y<<"!\", ";
            // shape
            fileDot << "shape = ellipse ]" <<std::endl;
        }
        for(size_t i=0; i<rectList.size(); i++) {
            // name
            fileDot << "\"" << rectList[i].myText << "\"";
            // position
            fileDot << "[ pos=\"" << rectList[i].center.x <<","<<-rectList[i].center.y<<"!\", ";
            // shape
            fileDot << "shape = box]" <<std::endl;
        }
        for(size_t i=0; i<diamondList.size(); i++) {
            // name
            fileDot << "\"" << diamondList[i].myText << "\"";
            // position
            fileDot << "[ pos=\"" << diamondList[i].center.x <<","<<-diamondList[i].center.y<<"!\", ";
            // shape
            fileDot << "shape = diamond]" <<std::endl;
        }

        //----------------
        // all relations
        //----------------
        for(size_t i=0; i<relationList.size(); i++) {
            fileDot << "\"" << relationList[i].parent.myText << "\"";
            fileDot << "->";
            fileDot << "\"" << relationList[i].child.myText << "\"";
            // if undirected graph
            if(relationList[i].type==0) {
                fileDot << "[dir=none]";
            }
            fileDot << std::endl;
        }
        
        //-----------
        // file end
        //-----------
        fileDot << "}" <<std::endl;
    } else {
        std::cout << "File: " << fName << " failed to open!" << std::endl;
        exit(1);
    }

    fileDot.close();
}
