//----------------------------------------------------------------------------
// This class is for detecting simple shapes of a diagram.
// Currently allowed shapes are:
//  [1] rectangles
//  [2] ellipses
//  [3] diamonds
//  [4] triangles (indicating direction at the end of curve)
//  [5] simple curves (straight lines or with a bit curvature)
//-------------
// Reference:
//-------------
//  [1] OPENCV/samples/cpp/squares.cpp
//  [2] OPENCV/samples/cpp/fitellipse.cpp
//  OPENCV stands for opencv installation folder
//  [3] http://bytefish.de/blog/extracting_contours_with_opencv/
//============================================================
// Weiran (Ryan) Zhao 
// Computer Science Dept, IU Bloomington
// Started: Sun,Apr 27th 2014 10:01:38 PM EDT
// Last Modified: Wed,May 07th 2014 08:30:52 PM EDT
//----------------------------------------------------------------------------
#include"shapeDetect.h"
#include<opencv2/core/core.hpp>
#include<opencv2/imgproc/imgproc.hpp>
#include<opencv2/highgui/highgui.hpp>
#include<cmath>
#include<algorithm>
#include<iostream>

// use wndName
extern char* wndName;

shapeDetect::shapeDetect(const cv::Mat &img) {
    this->processedImg = img;
}

shapeDetect::shapeDetect(const shapeDetect &sp) {
    this->processedImg = sp.processedImg;
}

shapeDetect& shapeDetect::operator= (const shapeDetect &sp) {
    this->processedImg = sp.processedImg;
    return *this;
}

void shapeDetect::getShapes() {
    std::vector< std::vector<cv::Point> > contours;
    cv::Mat binImg;
    //---------------------------------------------------------------------------
    // do this process multiple times, thresholding at different levels Ref [1]
    //---------------------------------------------------------------------------
    for(int l=0; l<BINARIZE_LEVELS; l++) {
       if(l==0) {
           // dilate image
           cv::dilate(processedImg, binImg, cv::Mat(), cv::Point(-1,-1));
           //binImg = processedImg;
       } else {
           // binarize image
           binImg = processedImg >=(l+1)*255/ BINARIZE_LEVELS;
       }
       //-------------------------------------------------------
       // find all contours. alternative: CV_CHAIN_APPROX_NONE
       //-------------------------------------------------------
       cv::findContours(binImg, contours, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE);

       //--------------------------------
       // loop through all the contours
       //--------------------------------
       std::vector<cv::Point> approx;
       for(size_t i=0; i<contours.size();i++) {
           // approximate the contour 
           cv::approxPolyDP(cv::Mat(contours[i]), approx, APPROX_ACCURACY*cv::arcLength(cv::Mat(contours[i]), true), true);
           if(isRectangle(approx)) {
               continue;
           }
           if(isDiamond(approx)) {
               continue;
           }
           if(isEllipse(contours[i])) {
               continue;
           }
       }
    }
}

bool shapeDetect::isEllipse(const std::vector<cv::Point>& cont) {
    if(cont.size()<=8) {
        return false;
    }
    // contour points in floats
    cv::Mat contf;
    cv::Mat(cont).convertTo(contf, CV_32F);
    //------------------
    // fit the contour
    //------------------
    cv::RotatedRect boundingBox = cv::fitEllipse(contf);
    //---------------------------------------------------------
    // calculate area using fitted ellipse and contour points
    //---------------------------------------------------------
    double fitArea = boundingBox.size.width*boundingBox.size.height*MY_PI/4;
    double contArea = fabs(cv::contourArea(cont, true));
    double ratio = fitArea/contArea;
    //std::cout << "fitArea: "<<fitArea << std::endl;
    //std::cout << "contArea: "<<contArea<< std::endl;
    //std::cout << "ratio: "<<ratio << std::endl;
    //------------------------------
    // decide if this is a ellipse
    //------------------------------
    if(contArea < processedImg.rows*processedImg.cols*ELLIPSE_AREA_MIN_RATIO) {
        return false;
    }
    if(fitArea > processedImg.rows*processedImg.cols*ELLIPSE_AREA_MAX_RATIO) {
        return false;
    }
    if((ratio > ELLIPSE_HIGH_RATIO) || (ratio < ELLIPSE_LOW_RATIO)) {
        return false;
    } else {
        myEllipse ellipse(boundingBox);
        // even this is a ellipse, we also need to decide whether to add to list
        for(size_t i=0;i<ellipseList.size();i++) {
            // if similar just return
            if(ellipseList[i].quiteSimilar(ellipse)) {
                return true;
            }
        }
        //-------------------------------
        // add this ellipse to the list
        //-------------------------------
        ellipseList.push_back(ellipse);
        return true;
    }
}

void shapeDetect::showShapes(const cv::Mat &img) {
    cv::Mat demoImg = cv::Mat::zeros(img.size(),CV_8UC3);
    cv::cvtColor(img, demoImg, CV_GRAY2RGB);
    //---------------
    // plot ellipse
    //---------------
    for(size_t i=0; i<ellipseList.size();i++) {
        cv::ellipse(demoImg, cv::RotatedRect(ellipseList[i]), cv::Scalar(0,0,255), 1, CV_AA);
        //------------------
        // show augmented 
        //------------------
        //cv::ellipse(demoImg, ellipseList[i].enlargedEllipse, cv::Scalar(0,0,255), 1, CV_AA);
    }

    //------------------
    // plot rectangles
    //------------------
    std::vector<cv::Point> tmpVert;
    int n; 
    const cv::Point *ptr;
    for(size_t i = 0; i < rectList.size(); i++ ) {
        tmpVert = rectList[i].vertices;
        ptr = &tmpVert[0];
        n = tmpVert.size();
        polylines(demoImg, &ptr, &n, 1, true, cv::Scalar(0,255,0), 0, CV_AA);
        //------------------
        // show augmented 
        //------------------
        //tmpVert = rectList[i].enlargedVertices;
        //ptr = &tmpVert[0];
        //n = tmpVert.size();
        //polylines(demoImg, &ptr, &n, 1, true, cv::Scalar(0,255,0), 0, CV_AA);
    }

    //---------------
    // plot diamond
    //---------------
    for(size_t i = 0; i < diamondList.size(); i++ ) {
        tmpVert = diamondList[i].vertices;
        ptr = &tmpVert[0];
        n = tmpVert.size();
        polylines(demoImg, &ptr, &n, 1, true, cv::Scalar(255,0,0), 0, CV_AA);
        //------------------
        // show augmented 
        //------------------
        //tmpVert = diamondList[i].enlargedVertices;
        //ptr = &tmpVert[0];
        //n = tmpVert.size();
        //polylines(demoImg, &ptr, &n, 1, true, cv::Scalar(255,0,0), 0, CV_AA);
    }
    std::cout << "Showing Detected Shapes" << std::endl;
    cv::imshow(wndName, demoImg);
    cv::waitKey();
    cv::imwrite("shapes.png", demoImg);
}

//--------------------------
// helper function Ref:[1]
//--------------------------
static double angle(cv::Point pt1, cv::Point pt2, cv::Point pt0 ) {
    double dx1 = pt1.x - pt0.x;
    double dy1 = pt1.y - pt0.y;
    double dx2 = pt2.x - pt0.x;
    double dy2 = pt2.y - pt0.y;
    return (dx1*dx2 + dy1*dy2)/sqrt((dx1*dx1 + dy1*dy1)*(dx2*dx2 + dy2*dy2) + 1e-10);
}

bool shapeDetect::isRectangle(const std::vector<cv::Point>& cont) {
    //-------------------------
    // simple checks Ref: [1]
    //-------------------------
    if(cont.size()!=4) {
        return false;
    }
    if(cv::isContourConvex(cv::Mat(cont)) == false) {
        return false;
    }
    double contArea = fabs(cv::contourArea(cv::Mat(cont), true));
    if(contArea < processedImg.rows*processedImg.cols*RECT_AREA_MIN_RATIO) {
        return false;
    }
    if(contArea> processedImg.rows*processedImg.cols*RECT_AREA_MAX_RATIO) {
        return false;
    }

    //---------------------------------------------
    // check angles of this quadrilateral Ref:[1]
    //---------------------------------------------
    double maxCos = 0;
    double tmp;
    for(int i=2; i<5; i++) {
        tmp = fabs(angle(cont[i%4], cont[i-2], cont[i-1]));
        maxCos = std::max(maxCos, tmp);
    }
    // check angles
    if(maxCos < RECT_MAX_COS) {
        // check if it is a similar one in the list
        myRect rectangle(cont); 
        for(size_t i=0;i<rectList.size();i++) {
            // if similar just return
            if(rectList[i].quiteSimilar(rectangle)) {
                return true;
            }
        }
        rectList.push_back(rectangle);
        return true;
    } else {
        return false;
    }
}

bool shapeDetect::isDiamond(const std::vector<cv::Point>& cont) {
    //-------------------------
    // simple checks Ref: [1]
    //-------------------------
    if(cont.size()!=4) {
        return false;
    }
    if(cv::isContourConvex(cv::Mat(cont)) == false) {
        return false;
    }
    double contArea = fabs(cv::contourArea(cv::Mat(cont), true));
    if(contArea < processedImg.rows*processedImg.cols*DIAMOND_AREA_MIN_RATIO) {
        return false;
    }
    if(contArea> processedImg.rows*processedImg.cols*DIAMOND_AREA_MAX_RATIO) {
        return false;
    }

    //------------------------------------
    // opposite angles should be similar
    //------------------------------------
    double ang1= 0, ang2=0;
    for(int i=2; i<4; i++) {
        ang1= fabs(angle(cont[i%4], cont[(i-2)%4], cont[(i-1)%4]));
        ang2= fabs(angle(cont[(i+2)%4], cont[i%4], cont[(i+1)%4]));
        ang1= acos(ang1)*180/MY_PI;
        ang2= acos(ang2)*180/MY_PI;
        //std::cout << "Ang1: "<<ang1<<" Ang2: "<<ang2 << std::endl;
        if(fabs(ang1-ang2) > DIAMOND_MAX_DEG) {
            return false;
        }
    }
    //------------------------------------------
    // otherwise, should consider as a diamond
    //------------------------------------------
    // check if it is a similar one in the list
    myDiamond diamond(cont); 
    for(size_t i=0;i<diamondList.size();i++) {
        // if similar just return
        if(diamondList[i].quiteSimilar(diamond)) {
            return true;
        }
    }
    diamondList.push_back(diamond);
    return true;
}

// reference [3]
cv::Mat shapeDetect::getMask(int type, bool enlarged) {
    // create result matrix
    cv::Mat mask= cv::Mat::zeros(processedImg.rows, processedImg.cols, CV_8U);
    //----------------------------------------------------------
    // intensity value for background and shape, based on type
    //----------------------------------------------------------
    int backgroundVal, shapeVal;
    switch(type) {
        case 0:
            backgroundVal = 255;
            shapeVal = 0;
            break;
        case 1:
            backgroundVal = 0;
            shapeVal = 255;
            break;
        default:
            std::cerr << "type value " << type << " not supported"<<std::endl;
            exit(1);
            break;
    }

    mask.setTo(cv::Scalar(backgroundVal));
    //cv::imshow(wndName, mask);
    //cv::waitKey();
    //---------------
    // ellipse mask
    //---------------
    for(size_t i=0; i<ellipseList.size();i++) {
        if(enlarged) {
            cv::ellipse(mask, ellipseList[i].enlargedEllipse, cv::Scalar(shapeVal), CV_FILLED, CV_AA);
        } else {
            cv::ellipse(mask, ellipseList[i], cv::Scalar(shapeVal), CV_FILLED, CV_AA);
        }
    }
    //cv::imshow(wndName, mask);
    //cv::waitKey();
    //------------------
    // rectangles mask
    //------------------
    std::vector<cv::Point> tmpVert;
    int n; 
    const cv::Point *ptr;
    for(size_t i = 0; i < rectList.size(); i++ ) {
        if(enlarged) {
            tmpVert = rectList[i].enlargedVertices;
        } else {
            tmpVert = rectList[i].vertices;
        }
        ptr = &tmpVert[0];
        n = tmpVert.size();
        fillPoly(mask, &ptr, &n, 1, cv::Scalar(shapeVal), CV_AA);
    }

    //cv::imshow(wndName, mask);
    //cv::waitKey();

    //---------------
    // diamond mask
    //---------------
    for(size_t i = 0; i < diamondList.size(); i++ ) {
        if(enlarged) {
            tmpVert = diamondList[i].enlargedVertices;
        } else {
            tmpVert = diamondList[i].vertices;
        }
        ptr = &tmpVert[0];
        n = tmpVert.size();
        fillPoly(mask, &ptr, &n, 1, cv::Scalar(shapeVal), CV_AA);
    }
    //cv::imshow(wndName, mask);
    //cv::waitKey();
    return mask;
}
