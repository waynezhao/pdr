//----------------------------------------------------------------------------
// This class is used for extracting text using *tesseract ocr* based on
// rectangular, diamond, and ellipse detected.
//============================================================
// Weiran (Ryan) Zhao 
// Computer Science Dept, IU Bloomington
// Started: Mon,Apr 28th 2014 12:29:00 PM EDT
// Last Modified: Wed,May 07th 2014 12:32:10 AM EDT
//----------------------------------------------------------------------------
#ifndef _TEXT_EXTRACT__H__
#define _TEXT_EXTRACT__H__ 
#include<vector>
#include"myShapes.h"
#include"shapeDetect.h"

class textExtract {
    public:
        std::vector<myEllipse> ellipseList;
        std::vector<myRect> rectList;
        std::vector<myDiamond> diamondList;
        cv::Mat orgImg;

        // constructor
        textExtract(const shapeDetect& detector, const cv::Mat& img);
        void getTexts(shapeDetect &detector);
};
#endif
